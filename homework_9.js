`use strict`;

/*******Варіант 1********/

let elem = document.querySelector('.theory');
let array = ["Kharkiv", "Kiev", "Borispol", "Irpin", "Odessa", "Lviv", "Dnipro"];

function newList(arr, parent = document.body){

  let ul = document.createElement('ul');
  let li = arr.map(item => `<li>${item}</li>`);
  ul.innerHTML += li.join(' ');
  parent.append(ul);

  ul.classList.add('newList');
}

newList(array, elem);

/*******Варіант 2********/

// let elem = document.querySelector(`.theory`);

// function newList(arr, place){
//   let ul = document.createElement("ul");
//     for (let item of arr) {
//     ul.innerHTML += `<li>${item}</li>`
//   };
//   ul.classList.add('newList');
//   place.append(ul);
// }

// newList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], elem);

/*****Таймер *****/


let timer;
let x = 3;

function countdown(){
  document.querySelector('.timer-seconds').innerHTML = x;
  x--;
  if (x < 0){
    clearTimeout(timer);
    document.body.innerHTML = '';
  }
  else {
    timer = setTimeout(countdown, 1000);
  }
};

countdown();













































